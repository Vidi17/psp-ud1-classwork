package es.vidal.practica2.ejercicio1;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Ejercicio1 {
    public static void main(String[] args) {
        
        final int WAIT_TIME = 2;

        List<String> comandos = getCommand(args);

        ProcessBuilder processBuilder = new ProcessBuilder(comandos);

        try {
            Process process = processBuilder.start();
            if (!process.waitFor(WAIT_TIME, TimeUnit.SECONDS)){
                System.err.println("Tiempo de ejecución agotado");
                System.exit(4);
            }else {
                if (process.exitValue() == 0){
                    printAndSaveChildProcessOutput(process);
                }else {
                    System.err.println("Error proceso hijo");
                    showChildError(process);
                    System.err.println(5);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("IO exception" + e.getMessage());
            System.exit(2);
        } catch (InterruptedException interruptedException){
            interruptedException.printStackTrace();
            System.err.println("Interrupted exception" + interruptedException.getMessage());
            System.exit(3);
        }
    }

    /**
     * Este método comprueba si se han introducido argumentos y de nos er así, los pide
     * @param args Los argumentos pasados por terminal
     * @return Un Arraylist con todos los comandos
     */
    public static List<String> getCommand (String[] args){
        if(args.length == 0){
            System.err.println("Introduce un commando");
            System.exit(1);
        }
        return new ArrayList<>(Arrays.asList(args));
    }

    /**
     * Este método lee línea a línea el error del proceso hijo
     * @param process El proceso hijo
     */
    private static void showChildError(Process process) {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()))){
            String line;
            while ((line = bufferedReader.readLine()) != null){
                System.err.println(line);
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Este método lee línea a línea el resultado de la ejecución del comando y lo guarda en un archivo txt
     * @param process El proceso hijo
     */
    private static void printAndSaveChildProcessOutput(Process process){
        String line;
        String output = "/home/batoi/Escritorio/output.txt";
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            FileWriter fileWriter = new FileWriter(output);){
            while ((line = bufferedReader.readLine()) != null){
                System.out.println(line);
                fileWriter.write(line + System.lineSeparator());
            }
        }catch (IOException e){
            System.err.println("IO Exception" + e.getMessage());
        }
        System.out.println("El proceso hijo se ha guardado en el archivo " + output + " correctamente");
    }
}
