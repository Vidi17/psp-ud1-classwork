package es.vidal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ejercicio3Minusculas {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        String word;
        boolean goOn = true;
        while (goOn && (word = bufferedReader.readLine()) != null) {
            if (word.equalsIgnoreCase("finalizar")) {
                goOn = false;
            } else {
                System.out.println(word.toLowerCase());
            }
        }
    }

}
